// app.service("categoryService", ["$http", "$q", function ($http, $q)
// {
//     var deferred = $q.defer();

//     $http.get(CONSTANT.ROOT_URI + "data/category.json").then(function (data)
//     {
//         deferred.resolve(data);
//     });

//     this.getCategories = function ()
//     {
//         return deferred.promise;
//     }
// }]);


app.factory("RestService", function($http,$q) {
  var service = {};
  var urlBase = CONSTANT.ROOT_URI + "/data";

  service.getCategories = function() {
    return $http.get(urlBase + "/category.json");
  };

  service.getTypes = function() {
    return $http.get(urlBase + "/cabinetType.json");
  };

  service.getJobs = function() {
    return options = $http.get(urlBase + "/job.json");
  };

  service.getCsvFile = function() {
    return options = $http.get(urlBase + "/sample output.csv");
  };


  return service;
});