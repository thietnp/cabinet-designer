// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());

// Place any jQuery/helper plugins in here.

// function S4() {
//     return (((1+Math.random())*0x10000)|0).toString(16).substring(1); 
// }
 
// function guidGenerator(){
// // then to call it, plus stitch in '4' in the third group
// return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0,3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
// }

// $.getScript("model/CabinetType.js", function(){

//    alert("Script loaded but not necessarily executed.");

// });


// function getGridHeight (totalPage, currentPage, pageSize, dataLen,rowHeight,headerHeight,footerHeight) {  
//         rowHeight = (rowHeight == null || rowHeight == undefined) ? 30 : rowHeight; // row height  
//         headerHeight = (headerHeight == null || headerHeight == undefined) ? 35 : headerHeight; // header height  
//         footerHeight = (footerHeight == null || footerHeight == undefined) ? 40 : footerHeight; // bottom scroll bar height  
//         totalH = 0;  
//         if (totalPage > 1) {  
//             if (currentPage < totalPage) {  
//                 totalH = pageSize * rowHeight + headerHeight + footerHeight;  
//             } else {  
//                 var lastPageSize = dataLen % pageSize;  
//                 if (lastPageSize === 0) {  
//                     totalH = pageSize * rowHeight + headerHeight + footerHeight;  
//                 } else {  
//                     totalH = lastPageSize * rowHeight + headerHeight + footerHeight;  
//                 }  
//             }  
//             console.log(totalH);  
//         } else {  
//             totalH = dataLen * rowHeight + headerHeight + footerHeight;  
//         }  
//         return (totalH) + 'px';  
// }; 
