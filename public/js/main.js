'use strict';
/**
 * Cabinets Web app
 * @author XuanNH <ColdboySpring@gmail.com>
 */

/**
 * Main AngularJS Web Application
 */




var app = angular.module('cabinet_app', [
    'ngRoute','ngClipboard'
    // 'angularFileUpload',
    // 'ui.bootstrap',
    // 'ui.grid.pagination',
    // 'ui.grid.selection',
    // 'ui.grid.edit',
    // 'ui.grid.rowEdit',
    // 'ui.grid.saveState'
]);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        // Home
        .when("/", { templateUrl: "partials/home.html" })
        // Pages
        // .when("/cabinet_type", { templateUrl: "partials/cabinet_type.html", controller: "cabinetTypeCtrl" })
        // .when("/cabinets", { templateUrl: "partials/cabinets.html", controller: "cabinetsCtrl" })
        // else 404
        .otherwise("/404", { templateUrl: "partials/404.html", controller: "pageCtrl" });
}]);

// app.config(function(injectables) { // provider & constants
//           // do configuration
//       })
// app.run(function(injectables) { // services & constants
//            // do initialization
// });

/**
* CONTROLLERS
*/
/**
* Base controller containing common functions
*/
app.controller('pageCtrl', ["$scope", "$rootScope", "RestService", 'ngClipboard', function ($scope, $rootScope, RestService,ngClipboard) {
    var _this = this;
    RestService.getTypes().then(
        function successCallback(response) {
            $scope.types = response.data;
            $scope.editCabinetType($scope.types[0].style);
        },
        function errorCallback(response) {
            console.log(response)
        }
    );

    RestService.getCategories().then(
        function successCallback(response) {
            $scope.groups = response.data;
            $scope.selectGroup($scope.groups[0]);
        },
        function errorCallback(response) {
            console.log(response)
        }
    );

    RestService.getJobs().then(
        function successCallback(response) {
            $scope.jobs = response.data;
        },
        function errorCallback(response) {
            console.log(response)
        }
    );

    $scope.changeName = function(){
        $rootScope.user.jobIdentification = $rootScope.user.identification
        + ($rootScope.user.firstName.replace(/\s/g, "").replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '') || '')
        + ($rootScope.user.lastName.replace(/\s/g, "").replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '') || '')
    }

    $scope.editCabinetType = function (model) {
        $scope.types.forEach(item => {
            item.style.isActive = false;
            item.material.isActive = false;
        });
        $scope.cabinetType = model;
        $scope.cabinetType.isActive = true;
        console.log(model)
    }

    $scope.selectGroup = function (model) {
        $scope.groups.forEach(item => {
            item.isActive = false;
        });
        $scope.group = model;
        $scope.group.isActive = true;
        console.log(model)
    }

    $scope.editJob = function (model) {
        $scope.currentJob = model; 
        $scope.editingJob = angular.copy(model);  
        console.log(model)
    }

    $scope.saveJob = function () {
        angular.merge($scope.currentJob ,$scope.editingJob);
    }

    $scope.createCsv = function () {
        RestService.getCsvFile().then(
            function successCallback(response) {
                $scope.csvContent = response.data;
            },
            function errorCallback(response) {
                console.log(response)
            }
        );
    } 

    $scope.copyCsv = function () {
        ngClipboard.toClipboard($scope.csvContent);
        alert('Copied!')
        
    } 

    $rootScope.CONSTANT = CONSTANT;
    $rootScope.user = {
        username: 'Admin',
        firstName:'John',
        lastName:'Doe',
        identification: ('00000000000' +  Math.floor(Math.random() * 10)),//Math.floor(Math.random() * 1000000000000),
        jobIdentification: '',
        address: 'Cecilia Chapman 711-2880 Nulla St. Mankato Mississippi 96522',
        phone: '(257) 563-7401',
        email: 'JohnDoe@domain.com'
    };
    $scope.changeName();

    console.log('base controller');
    $scope.cabinetType = null;
    $scope.groups = [];
    $scope.types = [];
    $scope.jobs = [];
    $scope.editingJob = null;
    $scope.csvContent  = null;


}]);


// /**
// * Sidebar controller
// */
// app.controller('sidebarCtrl', ["$scope", "$rootScope", "$filter", "RestService", function ($scope, $rootScope, $filter, RestService) {
//     var _this = this;
//     $scope.groups = null;
//     $scope.types = null;

//     _this.initData = function () {
//         RestService.getCategories().then(
//             function successCallback(response) {
//                 $scope.groups = response.data;
//                 var filterItems = $filter('filter')($scope.groups, { disabled: false });
//                 if (filterItems && filterItems.length > 0) {
//                     filterItems[0].open = true;
//                 }
//             },
//             function errorCallback(response) {
//             }
//         );

//         RestService.getTypes().then(
//             function successCallback(response) {
//                 // TODO: TEST CODE WITH guidGenerator
//                 response.data.forEach(item => {
//                     item.uuid = guidGenerator();
//                 });
//                 $scope.types = response.data;
//             },
//             function errorCallback(response) {
//             }
//         );
//     };

//     _this.initData();
//     //var selected = null;
//     $scope.selected = null;
//     $scope.editCabinetType = function (model) {
//         //angular.merge($rootScope.cabinetType ,item);
//         //$rootScope.cabinetType = angular.copy(item);
//         //selected = angular.copy(model);
//         $scope.$emit('editCabinetType', angular.copy(model));
//         console.log('editCabinetType');
//         console.log(model);
//         $scope.selected = angular.copy(model);
//     }

//     $scope.updateCabinetType = function (model) {

//         var filterItems = $filter('filter')($scope.types, { uuid: model.uuid });
//         if (filterItems && filterItems.length > 0) {
//             angular.merge(filterItems[0], model)
//         }
//         console.log('updateCabinetType');
//         console.log(model);
//     }

//     $scope.$on('updateCabinetType', function (event, data) {
//         $scope.updateCabinetType(data);
//     });

// }]);


// /**
//  * Cabinet type controller
//  */
// app.controller('cabinetTypeCtrl', function ($scope, $rootScope, $controller, $timeout, RestService, FileUploader, uiGridConstants) {
//     // instantiate base controller
//     $controller('pageCtrl', { $scope: $scope, $rootScope: $rootScope });

//     $rootScope.showHeader = false;
//     $rootScope.showFooter = false;
//     // $rootScope.CONSTANT = CONSTANT;
//     var _this = this;
//     _this.scope = $scope;


//     // VARIABLES
//     _this.tempCabinetType = null;

//     $scope.frmDisabled = true;
//     $scope.cabinetType = null;
//     $scope.categories = null;
//     $scope.gridHeight = '82px';


//     // Private methods
//     _this.initData = function () {
//         RestService.getCategories().then(
//             function successCallback(response) {
//                 $scope.categories = response.data;
//             },
//             function errorCallback(response) {
//                 console.error(response);
//             }
//         );
//     };

//     _this.initGrid = function () {

//         // Init Grid
//         $scope.gridOptions = {
//             showGridFooter: false,
//             showColumnFooter: false,
//             enableFiltering: false,
//             enableColumnMenus: false,
//             enablePaginationControls: false,
//             paginationPageSize: 10,
//             // enableCellEditOnFocus: true,
//             // showSelectionCheckbox: true,
//             selectedItems: $scope.selectedRows,
//             //gridFooterTemplate: '<div class="ui-grid-footer"><a id="btnAddOption" type="button" ng-disabled="frmDisabled" class="btn btn-xs btn-success btn-add-option pull-right" ng-click="grid.appScope.removeOption(row.entity)" ><i class="glyphicon glyphicon-plus"></i> Add option</a></div>',
//             columnDefs: [
//                 { name: 'uuid', enableCellEdit: false, visible: false },
//                 { name: 'name', displayName: 'Option Name' },
//                 { name: 'description', displayName: 'Extended Option Description' },
//                 {
//                     name: 'actions', displayName: 'Actions', enableCellEdit: false, width: '15%', enableSorting: false, cellTemplate: '<a id="btnEditOption" title="Edit this option" type="button" class="btn btn-xs btn-danger btn-delete-option" ng-click="grid.appScope.editOption(row.entity)" ><i class="glyphicon glyphicon-pencil"></i> Edit</a><a id="btnDeleteOption"  title="Delete this option" type="button" class="btn btn-xs btn-danger btn-delete-option" ng-click="grid.appScope.removeOption(row.entity)" ><i class="glyphicon glyphicon-remove"></i> Delete</a>'
//                 },
//             ],
//             //data : $scope.gridData

//         };

//         $scope.gridOptions.onRegisterApi = function (gridApi) {
//             $scope.gridApi = gridApi;
//         }

//         $scope.$on('$destroy', function () {
//             $scope.gridApi = null;
//         });
//     };

//     _this.initUploader = function () {
//         // init uploader
//         _this.uploader = $scope.uploader = new FileUploader({
//             url: CONSTANT.IMG_UPLOAD_URI
//         });
//         // uploader filters
//         _this.uploader.filters.push({
//             name: 'imageFilter',
//             fn: function (item /*{File|FileLikeObject}*/, options) {
//                 var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
//                 return CONSTANT.IMG_EXTENTION.indexOf(type) !== -1;
//             }
//         });

//         // uploader callbacks
//         // _this.uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
//         //     console.info('onWhenAddingFileFailed', item, filter, options);
//         // };
//         _this.uploader.onAfterAddingFile = function (fileItem) {
//             console.info('onAfterAddingFile', fileItem);
//             //console.info('onAfterAddingFile', fileItem._file);

//             _this.getPreviewImg(fileItem._file, function (url) {
//                 //important, this is promise so we have to apply the scope to update view
//                 $scope.$apply(function () {
//                     $scope.cabinetType.thumbnail = url;
//                 });
//             });


//         };
//         // _this.uploader.onAfterAddingAll = function(addedFileItems) {
//         //     console.info('onAfterAddingAll', addedFileItems);
//         // };
//         // _this.uploader.onBeforeUploadItem = function(item) {
//         //     console.info('onBeforeUploadItem', item);
//         // };
//         // _this.uploader.onProgressItem = function(fileItem, progress) {
//         //     console.info('onProgressItem', fileItem, progress);
//         // };
//         // _this.uploader.onProgressAll = function(progress) {
//         //     console.info('onProgressAll', progress);
//         // };
//         // _this.uploader.onSuccessItem = function(fileItem, response, status, headers) {
//         //     console.info('onSuccessItem', fileItem, response, status, headers);
//         // };
//         // _this.uploader.onErrorItem = function(fileItem, response, status, headers) {
//         //     console.info('onErrorItem', fileItem, response, status, headers);
//         // };
//         // _this.uploader.onCancelItem = function(fileItem, response, status, headers) {
//         //     console.info('onCancelItem', fileItem, response, status, headers);
//         // };
//         // _this.uploader.onCompleteItem = function(fileItem, response, status, headers) {
//         //     console.info('onCompleteItem', fileItem, response, status, headers);
//         // };
//         // _this.uploader.onCompleteAll = function() {
//         //     console.info('onCompleteAll');
//         // };
//         console.info('uploader', _this.uploader);
//     };

//     _this.getPreviewImg = function (file, callback) {
//         var reader = new FileReader();
//         reader.onload = (function (theFile) {
//             return function (e) {
//                 var base64String = e.target.result;
//                 //console.log(base64String);
//                 if (typeof callback == 'function' || callback instanceof Function) {
//                     callback(base64String)
//                 }
//             };
//         })(file);
//         reader.readAsDataURL(file);
//     }

//     _this.getGirdData = function (uuid) {
//         RestService.getOptions(uuid).then(
//             function successCallback(response) {
//                 $scope.gridOptions.data = response.data;

//                 //$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
//                 $scope.gridHeight = getGridHeight($scope.gridApi.pagination.getTotalPages(), $scope.gridApi.pagination.getPage(), $scope.gridOptions.paginationPageSize, $scope.gridOptions.data.length, 30, 35, 0);
//                 $timeout(function () {
//                     $scope.gridApi.grid.handleWindowResize();
//                     //$scope.gridApi.core.refresh();  
//                 });
//             },
//             function errorCallback(response) {
//                 console.error(response);
//             }
//         );
//     }
//     _this.getGirdClearData = function () {

//         $scope.gridOptions.data = null;
//         $scope.gridHeight = getGridHeight($scope.gridApi.pagination.getTotalPages(), $scope.gridApi.pagination.getPage(), $scope.gridOptions.paginationPageSize, $scope.gridOptions.data.length);
//     }

//     // Puplic methods
//     $scope.$on('editCabinetType', function (event, data) {
//         $scope.editCabinetType(angular.copy(data));
//         _this.getGirdData(data.uuid);
//     });

//     $scope.editCabinetType = function (model) {
//         $scope.frmDisabled = false;
//         _this.tempCabinetType = angular.copy(model);
//         $scope.cabinetType = model;

//     }

//     $scope.updateCabinetType = function (model) {
//         _this.tempCabinetType = model;
//         $scope.$broadcast('updateCabinetType', angular.copy(model));
//     };

//     $scope.resetCabinetType = function (cancel) {
//         if (cancel) {
//             $scope.frmDisabled = true;
//             $scope.cabinetType = null;
//             $scope.previewImg = null
//             _this.tempCabinetType = null;
//             _this.getGirdClearData();
//         } else {
//             $scope.cabinetType = angular.copy(_this.tempCabinetType);
//             _this.getGirdData();
//         }

//     };

//     $scope.upload = function () {
//         var input = $('#upload-cabinet-type');
//         input.click();
//     };

//     $scope.removeOption = function (entity) {
//         alert("remote " + JSON.stringify(entity));
//     };
//     $scope.editOption = function (entity) {
//         alert("edit " + JSON.stringify(entity));
//     };

//     $scope.addNewRow = function () {
//         $scope.gridOptions.data.push({ uuid: guidGenerator() });
//     };

//     _this.initUploader();
//     _this.initGrid();
//     _this.initData();
// });


// /**
//  * Cabinets controller
//  */
// app.controller('cabinetsCtrl', function ($scope, $rootScope, $controller, $timeout, RestService, uiGridConstants) {
//     // instantiate base controller
//     $controller('pageCtrl', { $scope: $scope, $rootScope: $rootScope });

//     $rootScope.showHeader = false;
//     $rootScope.showFooter = false;

//     var _this = this;
//     _this.scope = $scope;

//     _this.initGrid = function () {

//         // Init Grid
//         $scope.gridOptions = {
//             showGridFooter: false,
//             showColumnFooter: false,
//             enableFiltering: false,
//             enableColumnMenus: false,
//             enablePaginationControls: false,
//             paginationPageSize: 10,
//             // enableCellEditOnFocus: true,
//             // showSelectionCheckbox: true,
//             selectedItems: $scope.selectedRows,
//             //gridFooterTemplate: '<div class="ui-grid-footer"><a id="btnAddOption" type="button" ng-disabled="frmDisabled" class="btn btn-xs btn-success btn-add-option pull-right" ng-click="grid.appScope.removeOption(row.entity)" ><i class="glyphicon glyphicon-plus"></i> Add option</a></div>',
//             columnDefs: [
//                 { name: 'uuid', enableCellEdit: false, visible: false },
//                 { name: 'name', displayName: 'Option Name' },
//                 {
//                     name: 'value', displayName: 'Selection', editableCellTemplate: 'ui-grid/dropdownEditor', editDropdownValueLabel: 'gender', editDropdownOptionsArray: [{ id: 1, gender: 'male' },
//                     { id: 2, gender: 'female' }]
//                 }
//             ],
//         };

//         $scope.gridOptions.onRegisterApi = function (gridApi) {
//             $scope.gridApi = gridApi;
//         }

//         $scope.$on('$destroy', function () {
//             $scope.gridApi = null;
//         });
//     };

//     _this.getGirdData = function (uuid) {
//         RestService.getOptions(uuid).then(
//             function successCallback(response) {
//                 $scope.gridOptions.data = [
//                     { "name": "option1", "value": 1 },
//                     { "name": "option2", "value": 2 },
//                     { "name": "option3", "value": 3 }
//                 ];

//                 //$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
//                 $scope.gridHeight = getGridHeight($scope.gridApi.pagination.getTotalPages(), $scope.gridApi.pagination.getPage(), $scope.gridOptions.paginationPageSize, $scope.gridOptions.data.length, 35, 30, 0);
//                 $timeout(function () {
//                     $scope.gridApi.grid.handleWindowResize();
//                     //$scope.gridApi.core.refresh();  
//                 });
//             },
//             function errorCallback(response) {
//                 console.error(response);
//             }
//         );
//     }

//     _this.initGrid();
//     _this.getGirdData();
// });




/**
* DIRECTIVES
*/
// app.directive('ngThumb', ['$window', function($window) {
// 		var helper = {
//             support: !!($window.FileReader && $window.CanvasRenderingContext2D),
//             isFile: function(item) {
//                 return angular.isObject(item) && item instanceof $window.File;
//             },
//             isImage: function(file) {
//                 var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
//                 return CONSTANT.IMG_EXTENTION.indexOf(type) !== -1;
//             }
//         };
// 		return {
//             restrict: 'A',
//             template: '<canvas/>',
//             link: function(scope, element, attributes) {
//                 if (!helper.support) return;

//                 var params = scope.$eval(attributes.ngThumb);

//                 if (!helper.isFile(params.file)) return;
//                 if (!helper.isImage(params.file)) return;

//                 var canvas = element.find('canvas');
//                 var reader = new FileReader();

//                 reader.onload = onLoadFile;
//                 reader.readAsDataURL(params.file);

//                 function onLoadFile(event) {
//                     var img = new Image();
//                     img.onload = onLoadImage;
//                     img.src = event.target.result;
//                 }

//                 function onLoadImage() {
//                     var width = params.width || this.width / this.height * params.height;
//                     var height = params.height || this.height / this.width * params.width;
//                     canvas.attr({ width: width, height: height });
//                     canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
//                 }
//             }
//         };
// 	}]);

// app.directive('setGridHeight', function () {
//     return {
//         'scope': false,
//         'link': function (scope, element, attrs) { //https://www.c-sharpcorner.com/article/how-to-customize-ui-grid-height-dynamically/
//             /*Can Manipulate the height here*/
//             attrs.$set("style", "height: " + (scope.gridOptions.data.length * 30 + 82) + "px");
//         }
//     };
// })
