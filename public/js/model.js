class BaseModel{
    constructor(option) {
        this.uuid = guidGenerator();
    }
}

class Category extends BaseModel {
	constructor(option) {
        super(option);
        if(option){
		  this.name = option.name;
		  this.order = option.order;
		  this.cabinetTypes = option.cabinetTypes || [];
        }
	}
}


class Cabinet extends BaseModel {
  constructor(option) {
    super(option);
    if(option){
        this.name = option.name;
        this.imageUrl = option.imageUrl;
        this.thumbnailUrl = option.thumbnailUrl;

        this.minHeight = option.minHeight;
        this.maxHeight = option.maxHeight;

        this.minWidth = option.minWidth;
        this.maxWidth = option.maxWidth;

        this.isAltWidth = option.isAltWidth;
    	this.minAltWidth = option.minAltWidth;
        this.maxAltWidth = option.maxAltWidth;

        this.minDepth = option.minDepth;
        this.maxDepth = option.maxDepth;

        this.isAltDepth = option.isAltDepth;
    	this.minAltDepth = option.minAltDepth;
        this.maxAltDepth = option.maxAltDepth;
    }
  }
}

class CabinetType extends Cabinet {
	 constructor(option){
	 	super(option);
        if(option){
	 	 this.category = option.category || [];
        }
	 }
}